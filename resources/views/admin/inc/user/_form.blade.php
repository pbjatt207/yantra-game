@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  {{$message}}
</div>
@endif
@if(count($errors->all()))
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif

<div class="row">

  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('login', 'Enter login')}}
      {{Form::text('login', '', ['class' => 'form-control', 'placeholder'=>'Enter login','id'=>'title','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('password', 'Enter password')}}
      {{Form::text('password', '', ['class' => 'form-control', 'placeholder'=>'Enter password','id'=>'title','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('name', 'Enter name')}}
      {{Form::text('name', '', ['class' => 'form-control', 'placeholder'=>'Enter name','id'=>'title','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('email', 'Enter email')}}
      {{Form::email('email', '', ['class' => 'form-control', 'placeholder'=>'Enter email','id'=>'title','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('mobile', 'Enter mobile')}}
      {{Form::number('mobile', '', ['class' => 'form-control', 'placeholder'=>'Enter mobile','id'=>'title','required'=>'required'])}}
    </div>
  </div>
  <div class="col-lg-6">
    <div class="form-group">
      {{Form::label('role_id', 'Select Role')}}
      {{Form::select('role_id', $roleArr,'', ['class' => 'form-control', 'placeholder'=>'Enter mobile','id'=>'title','required'=>'required'])}}
    </div>
  </div>
</div>
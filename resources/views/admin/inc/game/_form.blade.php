

  @if($message = Session::get('error'))
   <div class="alert alert-danger alert-block">
     <button type="button" class="close" data-dismiss="alert">x</button>
     {{$message}}
   </div>
  @endif
  @if(count($errors->all()))
    <div class="alert alert-danger">
      <ul>
        @foreach($errors->all() as $error)
          <li>{{$error}}</li>
        @endforeach
      </ul>
    </div>
@endif
<div class="row">
   <div class="col-lg-6">
      <div class="form-group">
        {{Form::label('name', 'Enter name')}}
        {{Form::text('name', '', ['class' => 'form-control', 'placeholder'=>'Enter title','id'=>'name','required'=>'required'])}}
      </div>
      <!-- <div class="form-group">
        {{Form::label('description', 'Enter short description')}}
        {{Form::textarea('record[description]', '', ['class' => 'form-control','id'=>'description', 'placeholder'=>'Enter short description','rows'=>'4', 'col'=>'3'])}}
      </div> -->
    </div>
    <div class="col-lg-6">
    <input type="hidden" name="type" value="{{ $request->type }}">
      <div class="form-group">
        {{Form::label('image', 'Choose Image')}}
        <div class="">    
           {{ Form::file('image') }}
        </div>
      </div>
    </div>
  </div>

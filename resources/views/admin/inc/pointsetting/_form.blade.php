@if($message = Session::get('error'))
   <div class="alert alert-danger alert-block">
     <button type="button" class="close" data-dismiss="alert">x</button>
     {{$message}}
   </div>
@endif

@if(count($errors->all()))
    <div class="alert alert-danger">
      <ul>
        @foreach($errors->all() as $error)
          <li>{{$error}}</li>
        @endforeach
      </ul>
    </div>
@endif
<div class="row">
   <div class="col-lg-6">
      <div class="form-group">
        {{Form::label('point_value', 'Enter Point Value (vs ₹1)')}}
        {{Form::text('record1[point_value]', '', ['class' => 'form-control', 'placeholder'=>'Enter Point Value','id'=>'point_value'])}}
      </div>
      <div class="form-group">
        {{Form::label('use_point', 'Enter Use Point (%)')}}
        {{Form::text('record1[use_point]', '', ['class' => 'form-control', 'placeholder'=>'Enter Use Point (%)','id'=>'use_point'])}}
      </div>
    </div>
    <div class="col-lg-6">
      <div class="form-group">
        {{Form::label('max_points', 'Enter Max Points (use time)')}}
        {{Form::text('record1[max_points]','', ['class'=>'form-control', 'placeholder'=>'Enter Max Points', 'id'=>'max_points'])}}
      </div>
      <div class="form-group">
        {{Form::label('min_cart_amount', 'Enter Min Cart Amount (use time)')}}
        {{Form::text('record1[min_cart_amount]','', ['class'=>'form-control', 'placeholder'=>'Enter Min Cart Amount', 'id'=>'min_cart_amount'])}}
      </div>
    </div>
    <div class="col-lg-6">
      <div class="form-group">
        {{Form::label('accept_point', 'Enter Referal Code Point')}}
        {{Form::text('record[accept_point]','', ['class'=>'form-control', 'placeholder'=>'Enter Referal Code Point', 'id'=>'accept_point'])}}
      </div>
    </div>

  </div>

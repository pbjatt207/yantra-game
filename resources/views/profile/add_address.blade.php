@extends('frontend.layout.master')

@section('title', 'Add Address')

@section('contant')

		<main>
    <div class="main-part">
        <!-- Start Breadcrumb Part -->
        <section class="breadcrumb-part menu-part" data-stellar-offset-parent="true" data-stellar-background-ratio="0.5" style="background-image: url('{{ url('imgs/breadbg1.jpg') }}');">
            <div class="container">
                <div class="breadcrumb-inner">
                    <h2>Profile</h2>
					<a href="{{ url('/') }}">Home</a>
                    <a href="{{ route('profile') }}">Profile</a>
                    <a><span>Add Address</span></a>
                </div>
            </div>
        </section>
        <!-- End Breadcrumb Part -->
        <section class="home-icon login-register bg-skeen">
            <div class="icon-default icon-skeen">
                <img src="{{url('imgs/scroll-arrow.png')}}" alt="">
            </div>
            <div class="container">
                <div class="row">
                    @include('profile.sidebar')
                    <div class="col-sm-8">
						<div class="panel">
							<div class="panel-body">
								<a href="{{ route('address') }}" class="pull-right" title="Edit" data-toggle="tooltip"><i class="icon-map-marker" style="background: none"></i> View Address</a>
								<h3>Add Address</h3>
								{{ Form::open() }}
								@if (\Session::has('success'))
					                <div class="alert alert-success toast-msg" style="color: green">
					                    {!! \Session::get('success') !!}</li>
					                </div>
					            @endif

					            @if (\Session::has('danger'))
					                <div class="alert alert-danger toast-msg" style="color: red;">
					                    {!! \Session::get('danger') !!}</li>
					                </div>
					            @endif

								@if($message = Session::get('error'))
								   <div class="alert alert-danger alert-block">
								     <button type="button" class="close" data-dismiss="alert">x</button>
								     {{$message}}
								   </div>
								  @endif
								  @if(count($errors->all()))
								    <div class="alert alert-danger">
								      <ul>
								        @foreach($errors->all() as $error)
								          <li>{{$error}}</li>
								        @endforeach
								      </ul>
								    </div>
								@endif
								<div class="row">
									<div class="col-xs-6">
										{{ Form::label('country', 'Country Name') }}
										{{ Form::text('country', '', ['class' => '', 'placeholder' => 'Country Name', 'required', 'id' => 'country']) }}
									</div>
									<div class="col-xs-6">
										{{ Form::label('state', 'State Name') }}
										{{Form::select('state', $states,'0', ['class' => 'select-dropbox', 'id' => 'state', 'required'])}}
									</div>
								</div>
								<div class="row">
									<div class="col-xs-6">
										{{ Form::label('city', 'City Name') }}
										{{Form::select('city', $cities,'0', ['class' => 'select-dropbox', 'id' => 'city', 'required'])}}
									</div>
									<div class="col-xs-6">
										{{ Form::label('pincode', 'Pincode') }}
										{{ Form::number('pincode', '', ['class' => '', 'placeholder' => 'Pincode', 'required', 'id' => 'pincode']) }}
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										{{ Form::label('address', 'Address') }}
										{{ Form::text('address', '', ['class' => '', 'placeholder' => 'Address', 'required', 'id' => 'address']) }}
									</div>
								</div>
								<div class="form-group">
									{{ Form::submit('Add', ['class' => 'btn-main']) }}
								</div>
								{{ Form::close() }}
							</div>
						</div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
@stop

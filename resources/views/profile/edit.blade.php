@extends('frontend.layout.master')

@section('title', 'Edit Profile')

@section('contant')

		<main>
    <div class="main-part">
        <!-- Start Breadcrumb Part -->
        <section class="breadcrumb-part menu-part" data-stellar-offset-parent="true" data-stellar-background-ratio="0.5" style="background-image: url('{{ url('imgs/breadbg1.jpg') }}');">
            <div class="container">
                <div class="breadcrumb-inner">
                    <h2>Profile</h2>
					<a href="{{ url('/') }}">Home</a>
                    <a href="{{ route('profile') }}">Profile</a>
                    <a><span>Edit Profile</span></a>
                </div>
            </div>
        </section>
        <!-- End Breadcrumb Part -->
        <section class="home-icon login-register bg-skeen">
            <div class="icon-default icon-skeen">
                <img src="{{url('imgs/scroll-arrow.png')}}" alt="">
            </div>
            <div class="container">
                <div class="row">
                    @include('profile.sidebar')
                    <div class="col-sm-8">
						<div class="panel">
							<div class="panel-body">
								<a href="{{ route('profile') }}" class="pull-right" title="Edit" data-toggle="tooltip"><i class="icon-user" style="background: none"></i> View Profile</a>
								<h3>Edit Profile</h3>
								{{ Form::open() }}
								@if (\Session::has('success'))
					                <div class="alert alert-success toast-msg" style="color: green">
					                    {!! \Session::get('success') !!}</li>
					                </div>
					            @endif

					            @if (\Session::has('danger'))
					                <div class="alert alert-danger toast-msg" style="color: red;">
					                    {!! \Session::get('danger') !!}</li>
					                </div>
					            @endif

								@if($message = Session::get('error'))
								   <div class="alert alert-danger alert-block">
								     <button type="button" class="close" data-dismiss="alert">x</button>
								     {{$message}}
								   </div>
								  @endif
								  @if(count($errors->all()))
								    <div class="alert alert-danger">
								      <ul>
								        @foreach($errors->all() as $error)
								          <li>{{$error}}</li>
								        @endforeach
								      </ul>
								    </div>
								@endif
								<div class="row">
									<div class="col-xs-6">
										{{ Form::label('fname', 'First Name *') }}
										{{ Form::text('record[fname]', $user->fname, ['class' => '', 'placeholder' => 'First Name', 'required', 'id' => 'fname']) }}
									</div>
									<div class="col-xs-6">
										{{ Form::label('lname', 'Last Name') }}
										{{ Form::text('record[lname]', $user->lname, ['class' => '', 'placeholder' => 'Last Name', 'id' => 'lname']) }}
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										{{ Form::label('name', 'Display Name *') }}
										{{ Form::text('record[name]', $user->name, ['class' => '', 'placeholder' => 'Name', 'required', 'id' => 'name', 'readonly']) }}
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12">
										{{ Form::label('email', 'Email ID *') }}
										{{ Form::email('record[email]', $user->email, ['class' => '', 'placeholder' => 'Email ID', 'required', 'id' => 'email']) }}
									</div>
								</div>
								<div class="form-group">
									{{ Form::submit('Update', ['class' => 'btn-main']) }}
								</div>
								{{ Form::close() }}
							</div>
						</div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
@stop

<?php

use Illuminate\Support\Facades\Route;


Route::get('/foo', function () {
    \Artisan::call('passport:install');
    return 'clearede';
});
Route::get('/migrate', function(){
    \Artisan::call('migrate');
    return 'migrate';
});
Route::any('admin', function () {
    return false;
});


// Route::get('', 'HomeController@index')->name('home');
Route::group(['middleware' => 'guest', 'namespace' => 'admin'], function () {
    Route::any('/', 'UserController@index')->name('admin_login');
    Route::post('main/checklogin', 'UserController@checklogin');
});


Route::get('/passport', function() {
    $exitCode = Artisan::call('passport:install');
    return 'clearede';
});

        
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group(['namespace' => 'api'], function () {
    Route::post('/login', 'LoginController@login');
    
    Route::get('/page/{slug}', 'PagesController@page');
    Route::post('/win', 'GameController@win_user');
    Route::get('/winhistory', 'GameController@winHistory');

    Route::get('/cron-game', 'GameController@cron_game');
    
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('/game/{type}', 'GameController@game');
        Route::post('/user_win_history', 'GameController@userwinHistory');
        Route::post('/user_wallet', 'GameController@userWallet');
        Route::post('/editprofile', 'LoginController@edit_profile');
        Route::post('/changepassword', 'LoginController@change_password');
        Route::post('/user_place_point', 'GameController@userPlacepoint');
        Route::resource('placepoint', 'PlacepointController');
        
        Route::get('/logout', 'LoginController@logout');
        
    
        
    
    
    });
});

Route::group(['middleware' => 'auth:api', 'namespace' => 'api'], function () {
    

});
    




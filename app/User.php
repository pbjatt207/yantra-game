<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name', 'email', 'password',
    // ];
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ['wallet_point']; 

    public function getWalletPointAttribute()
    {
        $totalCredits = Model\Wallet::where('type', 'credit')->where('user_id', $this->id)->sum('amount');
        
        $totalDebits = Model\Wallet::where('type', 'debit')->where('user_id', $this->id)->sum('amount');
        $Total =  $totalCredits - $totalDebits;
        return $Total;
    }
    public function role()
    {
        return $this->hasOne('App\Model\Role', 'id', 'role_id');
    }
   
}

<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\Win;
use App\Game;
use App\Placepoint;
use App\Model\Timeslot;
use Illuminate\Http\Request;

class WinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Win::with('user','game')->get();
        $page  = "win_list";
        $title = "Win List";
        $data  = compact('lists', 'page', 'title');

        return view('admin.layout', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Win $winner)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Win  $winner
     * @return \Illuminate\Http\Response
     */
    public function show(Win $winner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Win  $winner
     * @return \Illuminate\Http\Response
     */
    public function edit(Win $winner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Win  $winner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Win $winner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Win  $winner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Win $winner)
    {
        $winner->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }

    public function destroyAll(Request $request)
    {
        $ids = $request->sub_chk;
        // dd($ids);
        Win::whereIn('id', $ids)->delete();
        return redirect()->back()->with('success', 'Success! Select record(s) have been deleted');
    }
    public function winHistory()
    {
        
        // $win_history = \DB::table('win')
        //     ->groupBy('date')
        //     ->pluck('date');
        
        // $response = [];
        // $i = 0;
        // foreach($win_history as $date) {
        //     $winns = \DB::table('win')
        //     ->select('game.name', 'game.type', 'game.image', 'win.timeslot_id AS timeslot')
        //     ->leftjoin('game','win.game_id' ,'game.id')
        //     ->where('date', $date)
        //     // ->where('game.type', request('type'))
        //     ->get();

        //     if($winns->count() > 0) {
        //         $response[$i]['date'] = $date;
        //         $response[$i]['data'] = $winns;
        //         $i++;
        //     }
        // }
        $win = Win::orderBy('date','desc')->with('game')->get();
        
        
        $page  = "win_history";
        $title = "Win List";
        $data  = compact('win', 'page', 'title');

        return view('admin.layout', $data);
    }
    public function pointHistory(Request $request)
    {
        
        $placepoint = Placepoint::groupBy('timeslot_id')->pluck('timeslot_id');
        $game = Game::where('type', $request->type)->get();
        
        $place_date = Placepoint::orderBy('date','desc')->groupBy('date')->pluck('date');
        // dd($place_date);
        $gameArr = [];
        foreach($place_date as $dt){

            foreach($placepoint as $pp){
                $key = 0;
                foreach($game as $gm){
                    $points = Placepoint::where('timeslot_id',$pp)->where('date',$dt)->where('date',$request->date)->where('game_id',$gm->id)->sum('point');
                    if($points>0){
                    $gameArr[$dt][$pp][$key]['game'] =  $gm;

                    $gameArr[$dt][$pp][$key]['point'] = $points;
                    $key++;
                    }
                }
            }
            
        }
        // echo "<pre>";
        // print_r($gameArr); die;
        $page  = "point_history";
        $title = "Point History";
        $data  = compact('game','placepoint','gameArr', 'page', 'title');

        return view('admin.layout', $data);
        
    }
}

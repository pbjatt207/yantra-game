<?php
namespace App\Http\Controllers\admin;

use Illuminate\Routing\Controller as BaseController;
use App\Game;
use App\User;
use App\Model\Wallet;
use App\Model\Winner;


class DashboardController extends BaseController
{
    public function index()
    {
    	

        $page = 'dashboard';
        $title = 'Master Admin Dashboard ';
        $game  = Game::count();
        $user  = User::count();
        $wallet  = Wallet::count();
        $winner  = Winner::count();
        $city_game = Game::where('type', 'city')->count();
        $yantra_game = Game::where('type', 'yantra')->count();
        $user_wallet = Wallet::with('user')->groupBy('user_id')->get();
        foreach($user_wallet as $uw){
            $user_credit = Wallet::where('type','credit')->where('user_id',$uw->user_id)->sum('amount');
            $user_debit = Wallet::where('type','debit')->where('user_id',$uw->user_id)->sum('amount');
            
            $uw->wallet = $user_credit - $user_debit;
            
        }
        $last_win = Winner::with('game')->where('type', 'yantra')->orderBy('id','desc')->latest()->paginate(5);
        $data = compact('page', 'title', 'game', 'city_game', 'yantra_game', 'user', 'wallet', 'winner','user_wallet','last_win');

        $data['last_games'] = Winner::where('type', 'yantra')->latest()->skip(0)->take(5)->get()->pluck('game_name', 'timeslot_id');

        return view('admin.layout', $data);
    }
}

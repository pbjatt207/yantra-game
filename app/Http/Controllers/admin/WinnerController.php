<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\Winner;
use App\Game;
use App\Placepoint;
use App\Model\Timeslot;
use Illuminate\Http\Request;

class WinnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Winner::with('game')->orderBy('id', 'desc')->paginate(10);
        
        $page  = "winner.list";
        $title = "Winner List";
        $data  = compact('lists', 'page', 'title');

        return view('admin.layout', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Winner $winner)
    {
        
        $type = $request->type ?? "yantra";
        $date = $request->date ?? date('Y-m-d', time());

        $request->merge(compact('type', 'date'));
        $request->flash();
        
        $edit = [];
        $request->flash();

        $page  = "winner.add";
        $title = "Winner Add";
        $game  = Game::where('type',$request->type)->pluck('name','id');
        // $timeslot = Timeslot::get();
        
        // foreach($game as $gm){
        //     foreach($timeslot as $t){
                
        //          $point = Placepoint::where('game_id', $gm->id)->get();
        //     }
           
        //     $gm->gm = $point;
        // }
        
        $gameArr = [
            // ''  => 'Select Game'
        ];

        
        // foreach($game as $c) {

        //     $gameArr[ $c->id ] = $c->name;
        // }
        $query = Timeslot::where('type',$request->type);
        if(!empty($request->start_time)) {
            $query->where('time', '>=', $request->start_time);
        }
        if(!empty($request->end_time)) {
            $query->where('time', '<=', $request->end_time);
        }
        $timeslot  = $query->get();
        foreach($timeslot as $ts){
            $gameArr[ $ts->id ][] = "Select Game";
            foreach($game as $game_id => $game_name){

                $point = Placepoint::where('timeslot_id', $ts->time)->where('game_id', $game_id)->sum('point');
                $gameArr[$ts->id][$game_id] = $game_name .' - ('. $point .') ' ;
            }
        }        

        if(!$timeslot->isEmpty() && !empty($request->date) && !empty($request->type)) {
            foreach ($timeslot as $key => $tslot) {
                $winner = Winner::where('date', $request->date)->where('type', $request->type)->where('timeslot_id', $tslot->time)->first();
                $timeslot[$key]->game_id = $winner->game_id ?? "";
            }

            // dd($timeslot->toArray());
        }

        
        $data  = compact('page', 'title','gameArr', 'timeslot', 'request', 'edit');

        return view('admin.layout',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'game' => 'required'
        ]);
        
        // dd($request->all());
        
        
        foreach($request->game as $timeslot_id => $game_id) {
            $t  = Timeslot::find($timeslot_id);
            // foreach($timeslot as $t){
                if(!empty($game_id)) {
                    Winner::updateOrCreate([
                            'timeslot_id' => $t->time,
                            'date'  => $request->date,
                            'type'  => $request->type
                        ],
                        [
                        'game_id' => $game_id,
                        'timeslot_id' => $t->time,
                        'date'  => $request->date,
                        'type'  => $request->type
                    ]);
                }
            // }
        }
        return redirect()->back()->with('success', 'Success! New record has been added.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Winner  $winner
     * @return \Illuminate\Http\Response
     */
    public function show(Winner $winner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Winner  $winner
     * @return \Illuminate\Http\Response
     */
    public function edit(Winner $winner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Winner  $winner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Winner $winner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Winner  $winner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Winner $winner)
    {
        $winner->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }

    public function destroyAll(Request $request)
    {
        $ids = $request->sub_chk;
        // dd($ids);
        Winner::whereIn('id', $ids)->delete();
        return redirect()->back()->with('success', 'Success! Select record(s) have been deleted');
    }

    public function current_timeslot() {
        $data = [];
        $types = ['yantra', 'city'];


        foreach ($types as $type) {
            $date     = date('Y-m-d', time());
            $timeslot = Timeslot::where('type', $type)->where('time', '>', date('H:i:s', time()))->orderBy('time')->first();
    
            if(empty($timeslot->time)) {
                $date = date("Y-m-d", strtotime("+1 days"));
                $timeslot = Timeslot::where('type', $type)->orderBy('time')->first();
            }
    
            $games = $game  = Game::where('type', $type)->pluck('name','id');
            
            $gameData = [];
            $totalPoints = 0;
            foreach ($games as $gameId => $game) {
                $points = Placepoint::where('game_id', $gameId)->where('timeslot_id', $timeslot->time)->where('date', $date)->sum('point');

                $gameData[$gameId]['timeslot_id'] = $timeslot->time;
                $gameData[$gameId]['name'] = $game;
                $gameData[$gameId]['points'] = $points;
                $gameData[$gameId]['selected'] = Winner::where('date', $date)->where('type', $type)->where('timeslot_id', $timeslot->time)->where('game_id', $gameId)->count();

                
                
                // $gameData[$gameId]['r_time'] = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
                
                $totalPoints += $points;
            }

            $current_time = time();
            $next_time = strtotime($date . ' ' . $timeslot->time);
            $pending_time =  $next_time - $current_time;
            // $hours = floor($pending_time / 3600);
            // $mins = floor($pending_time / 60 % 60);
            // $secs = floor($pending_time % 60);
    
            $data[$type] = [
                'time'  => date("F d, Y h:i A", strtotime($date.' '.$timeslot->time)),
                'date'  => $date,
                'games' => $gameData,
                'total_points' => $totalPoints,
                'r_time'    => $pending_time, // sprintf('%02d : %02d : %02d', $hours, $mins, $secs)
            ];
        }

        return response()->json($data);
    }
    
    public function allot_winner(Request $request, $game_id) {
        Winner::updateOrCreate(
            [
                'timeslot_id'   => $request->time,
                'date'          => $request->date,
                'type'          => $request->type
            ],
            [
                'timeslot_id'   => $request->time,
                'date'          => $request->date,
                'type'          => $request->type,
                'game_id'       => $game_id
            ]    
        );

        return redirect()->back()->with('success', 'Winning game alloted to current timeslot.');
    }
}

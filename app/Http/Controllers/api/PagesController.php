<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Model\Pages;
use Illuminate\Http\Request;


class PagesController extends Controller
{
    public function page( $slug )
    {
        $list = Pages::where('slug', $slug)->get();

        $re = [
            'page' => $list,
        ];

        return response()->json($re);
    }
    
}
